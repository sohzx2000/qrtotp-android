package com.fyp.qrtotp;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Size;
import android.util.SparseArray;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.WindowManager;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ScanActivity extends AppCompatActivity {
    private static final String TAG = "ScanActivity";

    /* Permissions */
    private final int REQUEST_PERMISSION_CAMERA = 0;

    /* Views in Layout */
    private SurfaceView surfaceView;

    private CameraSource cameraSource;
    private final int CAMERA_FACING = CameraCharacteristics.LENS_FACING_BACK;
    private Size previewSize;
    private boolean QRfound = false;
    private boolean surfaceCreated = false;
    private boolean cameraStarted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        // Find all views
        surfaceView = findViewById(R.id.surfaceView_preview);

        // when surface is created, set surfaceCreated to true
        surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                Log.d(TAG, "surfaceCreated: ");
                surfaceCreated = true;
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
            }
        });

        // Request camera permission and start camera
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA}, REQUEST_PERMISSION_CAMERA);
        } else {
            setUpCamera();
            startCameraSource();
        }
    }


    /**
     * Identifies the camera to use (rear) and the output size to use
     */
    private void setUpCamera() {
        CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);;
        try {
            for (String cameraId : cameraManager.getCameraIdList()) {
                CameraCharacteristics cameraCharacteristics = cameraManager.getCameraCharacteristics(cameraId);
                if (cameraCharacteristics.get(CameraCharacteristics.LENS_FACING) == CAMERA_FACING) {
                    StreamConfigurationMap streamConfigurationMap = cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

                    // get device Size
                    Size deviceSize = getDeviceSize();

                    // get output size matching textureview aspect ratio from available options
                    Size[] sizes = streamConfigurationMap.getOutputSizes(SurfaceTexture.class);
                    for (Size size : sizes)
                        Log.d(TAG, "Size: " + size);
                    Size aspectRatio = new Size(deviceSize.getHeight(), deviceSize.getWidth());
                    previewSize = chooseOptimalSize(sizes, deviceSize.getWidth(), deviceSize.getHeight(), 9999, 9999, aspectRatio);
                    Log.d(TAG, "Preview Size: " + previewSize);
                }
            }
        } catch (CameraAccessException e) {
            // TODO: 4/9/2019 handle failure to access camera
            e.printStackTrace();
        } catch (NullPointerException e) {
            // TODO: 4/9/2019 handle failure to access camera
            e.printStackTrace();
        }
    }


    /**
     * Starts Camera API for QR scanning
     */
    private void startCameraSource() {
        Log.d(TAG, "startCameraSource: STARTS");

        // Create the BarcodeDetector
        final BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(getApplicationContext())
                .setBarcodeFormats(Barcode.DATA_MATRIX | Barcode.QR_CODE)
                .build();

        if (!barcodeDetector.isOperational()) {
            Log.w(TAG, "Detector dependencies not loaded yet");
        } else {
            //Initialize camerasource to use high resolution and set Autofocus on.
            cameraSource = new CameraSource.Builder(getApplicationContext(), barcodeDetector)
                    .setFacing(CameraSource.CAMERA_FACING_BACK)
                    .setRequestedPreviewSize(previewSize.getWidth(), previewSize.getHeight())
                    .setAutoFocusEnabled(true)
                    .setRequestedFps(2.0f)
                    .build();

            /**
             * Add call back to SurfaceView and check if camera permission is granted.
             * If permission is granted we can start our cameraSource and pass it to surfaceView
             */
            surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder holder) {
                    Log.d(TAG, "surfaceCreated: ");
                    try {
                        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                            Log.d(TAG, "surfaceCreated: " + "during surface creation");
                            cameraSource.start(surfaceView.getHolder());
                            cameraStarted = true;
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                }

                /**
                 * Release resources for cameraSource
                 */
                @Override
                public void surfaceDestroyed(SurfaceHolder holder) {
                    cameraSource.stop();
                }
            });

            // Set the TextRecognizer's Processor.
            barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
                @Override
                public void release() {
                }

                /**
                 * Detect text obtained from QRcode into a stringBuilder
                 * which will then be set to the textView.
                 * */
                @Override
                public void receiveDetections(Detector.Detections<Barcode> detections) {
                    final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                    if (barcodes.size() != 0 && !QRfound){
                        for(int i=0;i<barcodes.size();i++){
                            Barcode barcode = barcodes.valueAt(i);
                            String barcodeText = barcode.rawValue;
                            if (barcodeText.startsWith("QRTOTP")) {
                                QRfound = true;
                                Log.d(TAG, "receivedDetections: " + barcodeText);
                                break;
                            }
                        }
                    }
                }
            });
        }

        // Starts cameraSource if surface has been created before surface callback added
        if (surfaceCreated) {
            try {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && !cameraStarted) {
                    Log.d(TAG, "startCameraSource: " + "after surface created");
                    cameraSource.start(surfaceView.getHolder());
                    cameraStarted = true;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Returns the Size of the device (including navigation bar)
     *
     * @return refer method description
     */
    private Size getDeviceSize() {
        int deviceWidth;
        int deviceHeight;

        WindowManager windowManager = (WindowManager) getApplication().getSystemService(Context.WINDOW_SERVICE);
        final Display display = windowManager.getDefaultDisplay();
        Point outPoint = new Point();
        if (Build.VERSION.SDK_INT >= 19) {
            // include navigation bar
            display.getRealSize(outPoint);
        } else {
            // exclude navigation bar
            display.getSize(outPoint);
        }
        if (outPoint.y > outPoint.x) {
            deviceHeight = outPoint.y;
            deviceWidth = outPoint.x;
        } else {
            deviceHeight = outPoint.x;
            deviceWidth = outPoint.y;
        }

        return new Size(deviceWidth, deviceHeight);
    }

    /**
     * Given {@code choices} of {@code Size}s supported by a camera, choose the smallest one that
     * is at least as large as the respective texture view size, and that is at most as large as the
     * respective max size, and whose aspect ratio matches with the specified value. If such size
     * doesn't exist, choose the largest one that is at most as large as the respective max size,
     * and whose aspect ratio matches with the specified value.
     *
     * @param choices           The list of sizes that the camera supports for the intended output
     *                          class
     * @param textureViewWidth  The width of the texture view relative to sensor coordinate
     * @param textureViewHeight The height of the texture view relative to sensor coordinate
     * @param maxWidth          The maximum width that can be chosen
     * @param maxHeight         The maximum height that can be chosen
     * @param aspectRatio       The aspect ratio
     * @return The optimal {@code Size}, or an arbitrary one if none were big enough
     */
    private Size chooseOptimalSize(Size[] choices, int textureViewWidth, int textureViewHeight, int maxWidth, int maxHeight, Size aspectRatio) {

        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Size> bigEnough = new ArrayList<>();
        // Collect the supported resolutions that are smaller than the preview Surface
        List<Size> notBigEnough = new ArrayList<>();
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        for (Size option : choices) {
            if (option.getWidth() <= maxWidth && option.getHeight() <= maxHeight &&
                    option.getHeight() == option.getWidth() * h / w) {
                if (option.getWidth() >= textureViewWidth &&
                        option.getHeight() >= textureViewHeight) {
                    bigEnough.add(option);
                } else {
                    notBigEnough.add(option);
                }
            }
        }

        // Pick the smallest of those big enough. If there is no one big enough, pick the
        // largest of those not big enough.
        if (bigEnough.size() > 0) {
            Log.d(TAG, "Choosing from big enough");
            return Collections.min(bigEnough, new CompareSizesByArea());
        } else if (notBigEnough.size() > 0) {
            Log.d(TAG, "Choosing from not big enough");
            return Collections.max(notBigEnough, new CompareSizesByArea());
        } else {
            Log.e(TAG, "Couldn't find any suitable preview size");
            return choices[0];
        }
    }


    /** --------------------------------------------- LISTENERS --------------------------------------------- **/
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_CAMERA && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "onRequestPermissionsResult: " + "camera permission granted");
            setUpCamera();
            startCameraSource();
        }
    }

    /** --------------------------------------------- CLASSES --------------------------------------------- **/


    public class CompareSizesByArea implements Comparator<Size> {
        @Override
        public int compare(Size lhs, Size rhs) {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }
    }
}
